/**
 * Concept of a State Machine and all constituent parts.
 */

#pragma once

#include "Parameters.hh"

/*
 * Passed into State Machine / State execution methods
 */
class ExecutionContext
{
private:
    const Parameters * theParametersP;
    const long         theCurrentTime;

public:
    ExecutionContext(
            const Parameters * parametersP,
            const long         currentTime )
        :
            theParametersP( parametersP ),
            theCurrentTime( currentTime )
    {}

    const Parameters * getParametersP() const
    {
        return theParametersP;
    }

    long getCurrentTime() const
    {
        return theCurrentTime;
    }
};

/*
 * Abstract notion of a State in the State Machine. This is concrete
 * so it can be easily subclassed.
 */
class State
{
protected:
    const char * theName;

public:
    State(  const char * name );

    const char * getName() const {
        return theName;
    }

    virtual void enter(
            const ExecutionContext * contextP );

    virtual void execute(
            const ExecutionContext * contextP );

    virtual void leave(
            const ExecutionContext * contextP );

    virtual ~State();
};

/*
 * Abstract notion of a State that contains other States, exactly one of which
 * is active e at any time.
 */
class HierarchicalState :
    public State
{
protected:
    State * theChildStateP;

    virtual void transitionTo(
            State                  * newStateP,
            const ExecutionContext * contextP );

public:
    HierarchicalState(
            const char * name,
            State      * childStateP );

    virtual void enter(
            const ExecutionContext * contextP );

    virtual void execute(
            const ExecutionContext * contextP );

    virtual void leave(
            const ExecutionContext * contextP );
};

/*
 * Abstract notion of a State that contains other States, all of which
 * are active at any time.
 */
class ParallelState :
    public State
{
public:
    ParallelState(
            const char * name );
};

/*
 * A State Machine has exactly one root State
 */
class StateMachine
{
private:
    State * theRootStateP;

public:
    StateMachine(
            State * rootStateP )
        :
            theRootStateP( rootStateP )
    {}

    void init(
            const ExecutionContext * contextP )
    {
        theRootStateP->enter( contextP );
    }

    void execute(
            const ExecutionContext * contextP )
    {
        theRootStateP->execute( contextP );
    }
};

